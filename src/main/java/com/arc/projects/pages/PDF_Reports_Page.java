package com.arc.projects.pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arc.projects.utils.randomFileName;
import com.arcautoframe.utils.Log;



public class PDF_Reports_Page extends LoadableComponent<PDF_Reports_Page> {
	
	WebDriver driver;
	private boolean isPageLoaded;
	ProjectDashboardPage projectDashboardPage;
	FolderPage folderPage;
	
/** Identifying web elements using FindBy annotation*/
	
	@FindBy(css = ".dropdown-toggle.aPunchrfimenu")
	WebElement btnPrjManagement;
	@FindBy(css = "#a_pdfReport")
	WebElement tabReports;
	@FindBy(xpath="//a[@aria-label='addTemplate']")
	WebElement btnAddTemplate;
	@FindBy(xpath="//button[contains(text(),'Choose a file')]")
	WebElement btnChooseFileUploadTemplt;
	@FindBy(xpath="//div/input[3]")
	WebElement infoAttachedPdf;
	@FindBy(xpath="//input[@aria-label='templatename']")
	WebElement txtTemplateName;
	@FindBy(xpath="//input[@class='form-control ng-untouched ng-pristine ng-valid']")
	WebElement txtTemplateName_BeforeEnter;
	@FindBy(xpath="(//input[@type='text'])[5]")
	WebElement txtTemplateName_Edit;
	@FindBy(xpath="//select[@aria-label='requiredapprovals']")
	WebElement dropBtnrequiredapprovals;
	@FindBy(xpath="(//input[@type='text'])[6]")
	WebElement editApproveReject;
	@FindBy(xpath="(//input[@type='text'])[7]")
	WebElement editUserToNotify;
	@FindBy(xpath="//div[@class='ng2-menu-item ng2-menu-item--selected']")
	WebElement SelectUserToNotify;
	@FindBy(xpath="//button[contains(text(),'Save')]")
	WebElement btnSaveUploadTemplate;
	@FindBy(xpath="(//h4[@class='media-heading' and @aria-label='TemplateName'])[1]")
	WebElement TemplateName_List;
	@FindBy(xpath="(//span[@class='temp-approver'])[1]")
	WebElement ApprovalType_List;
	@FindBy(css="#yes")
	WebElement btnYes;
	@FindBy(xpath="(//span[@class='icon icon-pr-delete'])[1]")
	WebElement iconDeleteTemplate;
	@FindBy(css="#snackbar")
	WebElement infoNotifyMsg;
	@FindBy(xpath="//button[contains(text(),'Replace')]")
	WebElement btnReplaceFileEditTemp;
	@FindBy(xpath="//p[2]")
	WebElement infoTemplateName_Replace;
	@FindBy(xpath="//button[contains(text(),'Submit')]")
	WebElement btnSubmitReport;
	@FindBy(xpath="//button[contains(text(),'Draft')]")
	WebElement btnDraftReport;
	@FindBy(xpath="//input[@class='form-control ng-untouched ng-pristine']")
	WebElement txtReportNumber;
	@FindBy(xpath="//input[@class='form-control ng-untouched ng-pristine ng-valid']")
	WebElement txtReportDate;
	@FindBy(xpath="//button[@class='btn btn-outline btn-approve']")
	WebElement status_approved;
	@FindBy(xpath="//button[@class='btn btn-outline btn-pending']")
	WebElement status_Pending;
	@FindBy(xpath="(//span[@class='span-seperator'])[1]")
	WebElement ReportNO_approved;
	@FindBy(xpath="(//span[@class='span-seperator'])[2]")
	WebElement ReportNO_BeforeApproveReject;
	@FindBy(xpath="(//p[@class='dv-right pull-right']/span)[1]")
	WebElement ReportStatus_WhileApproveORReject;
	@FindBy(xpath="(//i[@class='icon icon-pr-download'])[2]")
	WebElement iconDownload_PhotoAttached;
	@FindBy(xpath="(//i[@class='icon icon-pr-download'])[3]")
	WebElement iconDownload_FileAttached;
	@FindBy(xpath="//button[contains(text(),'Approve')]")
	WebElement btn_Approve;
	@FindBy(xpath="//button[contains(text(),'Reject')]")
	WebElement btn_Reject;
	@FindBy(xpath="//i[@class='icon icon-pr-tags-collapse-arrow']")
	WebElement icon_ApproversDetails_Report;
	@FindBy(xpath="//div[@class='division-body']/span")
	WebElement info_ApproverDetails;
	@FindBy(xpath="(//a[@class='dropdown-toggle' and @aria-haspopup='true'])[1]")
	WebElement btn_AddPhotos_Report;
	@FindBy(xpath="(//a[contains(text(),'Computer')])[1]")
	WebElement icon_AddPhotos_Computer;
	@FindBy(xpath="//a/i[@class='icon icon-pr-skysite-gallery']")
	WebElement icon_AddPhotos_SkySite;
	@FindBy(xpath="(//a[@class='dropdown-toggle' and @aria-haspopup='true'])[2]")
	WebElement btn_LinkedFiles_Report;
	@FindBy(xpath="(//a[contains(text(),'Computer')])[2]")
	WebElement icon_LinkFiles_Computer;
	@FindBy(xpath="//div[@class='tag-file-card padding-right-0']")
	WebElement Attached_Photos_Info;
	@FindBy(xpath="//textarea[@class='form-control ng-untouched ng-pristine ng-valid']")
	WebElement txt_AddActivityComment;
	@FindBy(xpath="//button[@class='btn btn-default btn-widthless']")
	WebElement btn_AddComment_Submit;
	@FindBy(xpath="//div[@class='chat-body clearfix']")
	WebElement info_AddedCommentProof;
	@FindBy(xpath="(//div[@class='tag-file-card padding-right-0'])[2]")
	WebElement Attached_Files_Info;
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, btnAddTemplate, 20);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 * @return 
	 */
	public PDF_Reports_Page(WebDriver driver) 
	{
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
	}
	
	/**
	 * Method written for Select Reports List from Project Management Tab
	 * Scipted By: Naresh Babu
	 * @return
	 */
	public FolderPage SelectPDFReportsTab() 
	{
		SkySiteUtils.waitForElement(driver, btnPrjManagement, 30);
		btnPrjManagement.click();
		Log.message("Clicked on Project Management tab.");
		SkySiteUtils.waitForElement(driver, tabReports, 20);
		SkySiteUtils.waitTill(2000);
		tabReports.click();
		Log.message("Clicked on Reports List tab.");
		SkySiteUtils.waitForElement(driver, btnAddTemplate, 20);
		if (btnAddTemplate.isDisplayed())
			Log.message("Add template button is available.");
		return new FolderPage(driver).get();
	}
	
	/**
	 * Method written for Add template with PDF attachment
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws AWTException 
	 */
	@FindBy(xpath="//select[@class='form-control select__field' and @aria-label='requiredapprovals']")
	WebElement DropDown_RequiredApproval;
	public boolean Add_Template_WithPDFAttachment_AndValidate(String FolderPath,String Randome_TemplateName,String Input_ApprovalType,String Approver_MailId,String Notify_MailID) throws IOException, AWTException 
	{
		boolean result = false;
		SkySiteUtils.waitForElement(driver, btnAddTemplate, 30);
		btnAddTemplate.click();
		Log.message("Clicked on Add Template button.");
		SkySiteUtils.waitForElement(driver, btnChooseFileUploadTemplt, 30);
		SkySiteUtils.waitTill(5000);
		btnChooseFileUploadTemplt.click();
		Log.message("Clicked on Choose a file button");
		SkySiteUtils.waitTill(10000);
		// Writing File names into a text file for using in AutoIT Script
		BufferedWriter output;
		randomFileName rn = new randomFileName();
		String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
		output = new BufferedWriter(new FileWriter(tmpFileName, true));
		String expFilename = null;
		File[] files = new File(FolderPath).listFiles();
		for (File file : files) {
		if (file.isFile()) {
			expFilename = file.getName();// Getting File Names into a variable
			Log.message("Expected File name is:" + expFilename);
			output.append('"' + expFilename + '"');
			output.append(" ");
			SkySiteUtils.waitTill(1000);
			}
		}
		output.flush();
		output.close();
		// Executing .exe autoIt file
		String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
		Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
		Log.message("AutoIT Script Executed!!");
		SkySiteUtils.waitTill(10000);			
		SkySiteUtils.waitForElement(driver, infoAttachedPdf, 30);
		//Delete the temp file
		try
		{
			File file = new File(tmpFileName);
			if(file.delete())
			{
				Log.message(file.getName() + " is deleted!");
			}
			else
			{
				Log.message("Delete operation is failed.");
			}
		}
		catch(Exception e)
		{
			Log.message("Exception occured!!!"+e);	
		}
		String Act_TempltName_AftrSelect = txtTemplateName.getAttribute("value");
		Log.message("Template name after select a PDF is : "+Act_TempltName_AftrSelect);
		txtTemplateName.clear();
		SkySiteUtils.waitTill(2000);
		txtTemplateName.sendKeys(Randome_TemplateName);
		Log.message("Template name was entered as : "+Randome_TemplateName);
		SkySiteUtils.waitTill(2000);
		//Select Required approvals based on the user input
		if(Input_ApprovalType.equals("No approval required"))
		{
			Log.message("No need to select any approval.");
			//SkySiteUtils.waitForElement(driver, editUserToNotify, 30);
			//SkySiteUtils.waitTill(2000);
			//editUserToNotify.click();
			//SelectUserToNotify.click();
			//editUserToNotify.sendKeys(Notify_MailID);
			//Log.message("Selected User to notify mail ID.");
			SkySiteUtils.waitTill(2000);
		}
		if((Input_ApprovalType.equals("Anyone can approve"))||(Input_ApprovalType.equals("All must approve")))
		{
			Select Select_ApprovalType = new Select(DropDown_RequiredApproval);
			Select_ApprovalType.selectByVisibleText(Input_ApprovalType);
			Log.message("Required approval selected from list.");
			SkySiteUtils.waitForElement(driver, editApproveReject, 30);	
			SkySiteUtils.waitTill(5000);
			editApproveReject.click();
			SkySiteUtils.waitTill(2000);
			editApproveReject.sendKeys(Approver_MailId);
			SkySiteUtils.waitTill(8000);
			driver.findElement(By.xpath("//b")).click();
			Log.message("Entered 1st approver, rejecter mail ID.");
			SkySiteUtils.waitTill(5000);
			editApproveReject.click();
			SkySiteUtils.waitTill(2000);
			editApproveReject.sendKeys(Notify_MailID);
			SkySiteUtils.waitTill(8000);
			driver.findElement(By.xpath("//b")).click();
			Log.message("Entered 2nd approver, rejecter mail ID.");
			SkySiteUtils.waitTill(5000);
			/*editUserToNotify.clear();
			SkySiteUtils.waitTill(2000);
			editUserToNotify.sendKeys(Notify_MailID);
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("//b")).click();
			SkySiteUtils.waitTill(2000);
			//editUserToNotify.click();
			Log.message("Entered User to notify mail ID.");
			//SkySiteUtils.waitTill(10000);
*/		}
		btnSaveUploadTemplate.click();
		Log.message("Clicked on Save button after filled all details.");
		SkySiteUtils.waitTill(40000);
		SkySiteUtils.waitForElement(driver, TemplateName_List, 30);
		//Counting available list of templates
		int NoOf_Templates = driver.findElements(By.xpath("//h4[@class='media-heading' and @aria-label='TemplateName']")).size();
		Log.message("Available Templates are : "+NoOf_Templates);
		int i = 0;
		for(i=1;i<=NoOf_Templates;i++)
		{
			String Act_TempName = driver.findElement(By.xpath("(//h4[@class='media-heading' and @aria-label='TemplateName'])["+i+"]")).getText();
			Log.message("Template after create is: "+Act_TempName);
			String Act_ApprovalType = driver.findElement(By.xpath("(//span[@class='temp-approver'])["+i+"]")).getText();
			Log.message("Template Approval type is: "+Act_ApprovalType);
			if ((Act_TempName.trim().contentEquals(Randome_TemplateName)) && (Act_ApprovalType.trim().contentEquals(Input_ApprovalType))) 
			{
				Log.message("Template is available.");
				result = true;
				break;
			} 
		}
		if (result == true) 
		{
			Log.message("Adding template is working successfully.");
			return true;
		} else {
			Log.message("Adding template is failed.");
			return false;
		}
	}
	
	/**
	 * Method written for Create report - No Approval required
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws AWTException 
	 */
	public boolean Create_Report_NoApprovalRequired(String Randome_TemplateName,String Input_ApprovalType) throws IOException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		//Counting available list of templates
		int NoOf_Templates = driver.findElements(By.xpath("//h4[@class='media-heading' and @aria-label='TemplateName']")).size();
		Log.message("Available Templates are : "+NoOf_Templates);
		int i = 0;
		for(i=1;i<=NoOf_Templates;i++)
		{
			String Act_TempName = driver.findElement(By.xpath("(//h4[@class='media-heading' and @aria-label='TemplateName'])["+i+"]")).getText();
			Log.message("Template after create is: "+Act_TempName);
			String Act_ApprovalType = driver.findElement(By.xpath("(//span[@class='temp-approver'])["+i+"]")).getText();
			Log.message("Template Approval type is: "+Act_ApprovalType);
			if ((Act_TempName.trim().contentEquals(Randome_TemplateName)) && (Act_ApprovalType.trim().contentEquals(Input_ApprovalType))) 
			{
				Log.message("Template is available.");
				result1 = true;
				break;
			} 
		}
		
		if (result1 == true) 
		{
			Log.message("Expected Template is available. User can Create and Submit.");
			driver.findElement(By.xpath("(//button[@class='btn btn-primary' and @routerlink='/createReport'])["+i+"]")).click();
			Log.message("Clicked on Create button.");
			SkySiteUtils.waitForElement(driver, btnSubmitReport, 60);
			SkySiteUtils.waitTill(15000); 
			String Report_Number_Auto = txtReportNumber.getAttribute("value");
			Log.message("Report number is: "+Report_Number_Auto);
			String Date_Auto = txtReportDate.getAttribute("value");
			Log.message("Auto selected date is: "+Date_Auto);
			SkySiteUtils.waitTill(1000);
			btnSubmitReport.click();
			Log.message("Clicked on Submit button.");
			SkySiteUtils.waitForElement(driver, btnYes, 20);
			btnYes.click();
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, status_approved, 20);
			String Act_ReportNO_SubmittedList = ReportNO_approved.getText();
			Log.message("Report NO after submit is: "+Act_ReportNO_SubmittedList);
			if((Report_Number_Auto.contentEquals("1")) && (Act_ReportNO_SubmittedList.contains("1")))
			{
				result2 = true;
				Log.message("Report details are proper and status was approved.");
			}
			else
			{
				result2 = false;
				Log.message("Report details are not proper.");
			}
		} 
		if((result1==true) && (result2==true))
		{
			Log.message("User successfully submit the report.");
			return true;
		}
		else
		{
			Log.message("User Unable to submit the report.");
			return false;
		}
	}
	
	/**
	 * Method written for Create report - with Approval required
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws AWTException 
	 */
	public boolean Create_Report_WithApprovalRequired(String Randome_TemplateName,String Input_ApprovalType) throws IOException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		//Counting available list of templates
		int NoOf_Templates = driver.findElements(By.xpath("//h4[@class='media-heading' and @aria-label='TemplateName']")).size();
		Log.message("Available Templates are : "+NoOf_Templates);
		int i = 0;
		for(i=1;i<=NoOf_Templates;i++)
		{
			String Act_TempName = driver.findElement(By.xpath("(//h4[@class='media-heading' and @aria-label='TemplateName'])["+i+"]")).getText();
			Log.message("Template after create is: "+Act_TempName);
			String Act_ApprovalType = driver.findElement(By.xpath("(//span[@class='temp-approver'])["+i+"]")).getText();
			Log.message("Template Approval type is: "+Act_ApprovalType);
			if ((Act_TempName.trim().contentEquals(Randome_TemplateName)) && (Act_ApprovalType.trim().contentEquals(Input_ApprovalType))) 
			{
				Log.message("Template is available.");
				result1 = true;
				break;
			} 
		}
		
		if (result1 == true) 
		{
			Log.message("Expected Template is available. User can Create and Submit.");
			driver.findElement(By.xpath("(//button[@class='btn btn-primary' and @routerlink='/createReport'])["+i+"]")).click();
			Log.message("Clicked on Create button.");
			SkySiteUtils.waitForElement(driver, btnSubmitReport, 60);
			SkySiteUtils.waitTill(20000); 
			String Report_Number_Auto = txtReportNumber.getAttribute("value");
			Log.message("Report number is: "+Report_Number_Auto);
			String Date_Auto = txtReportDate.getAttribute("value");
			Log.message("Auto selected date is: "+Date_Auto);
			SkySiteUtils.waitTill(1000);
			btnSubmitReport.click();
			Log.message("Clicked on Submit button.");
			SkySiteUtils.waitForElement(driver, btnYes, 20);
			btnYes.click();
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, status_Pending, 20);
			String Act_ReportNO_SubmittedList = ReportNO_approved.getText();
			Log.message("Report NO after submit is: "+Act_ReportNO_SubmittedList);
			if((Report_Number_Auto.contentEquals("1")) && (Act_ReportNO_SubmittedList.contains("1")))
			{
				result2 = true;
				Log.message("Report details are proper and status was Pending.");
			}
			else
			{
				result2 = false;
				Log.message("Report details are not proper.");
			}
		} 
		if((result1==true) && (result2==true))
		{
			Log.message("User successfully submit the report.");
			return true;
		}
		else
		{
			Log.message("User Unable to submit the report.");
			return false;
		}
	}
	
	/**
	 * Method written for Create & Submit report with attachments and comments from approver side
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws AWTException 
	 */
	public boolean Create_Submit_FMApproverSide_WithCommentAndAttachments(String Randome_TemplateName,String Input_ApprovalType,String Act_ReportNO,String CreatorName,String Approver_Name,String Exp_ReportNO) throws IOException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		//Counting available list of templates
		int NoOf_Templates = driver.findElements(By.xpath("//h4[@class='media-heading' and @aria-label='TemplateName']")).size();
		Log.message("Available Templates are : "+NoOf_Templates);
		int i = 0;
		for(i=1;i<=NoOf_Templates;i++)
		{
			String App_ReportNO = driver.findElement(By.xpath("(//span[@class='text-dark'])[1]")).getText();
			Log.message("present Report number before submit from approver side is: "+App_ReportNO);
			String App_Creator_Name = driver.findElement(By.xpath("(//span[@class='text-dark'])[2]")).getText();
			Log.message("Creater name is: "+App_Creator_Name);
			String Act_TempName = driver.findElement(By.xpath("(//h4[@class='media-heading' and @aria-label='TemplateName'])["+i+"]")).getText();
			Log.message("Template after create is: "+Act_TempName);
			String Act_ApprovalType = driver.findElement(By.xpath("(//span[@class='temp-approver'])["+i+"]")).getText();
			Log.message("Template Approval type is: "+Act_ApprovalType);
			if ((App_ReportNO.contentEquals(Act_ReportNO)) && (App_Creator_Name.contains(CreatorName)) &&
				(Act_TempName.trim().contentEquals(Randome_TemplateName)) && (Act_ApprovalType.trim().contentEquals(Input_ApprovalType))) 
			{
				Log.message("Template is available with expected details.");
				result1 = true;
				break;
			} 
		}
		
		if (result1 == true) 
		{
			Log.message("Expected Template is available. User can Create and Submit.");
			driver.findElement(By.xpath("(//button[@class='btn btn-primary' and @routerlink='/createReport'])["+i+"]")).click();
			Log.message("Clicked on Create button.");
			SkySiteUtils.waitForElement(driver, btnSubmitReport, 60);
			SkySiteUtils.waitTill(15000); 
			String Report_Number_Auto = txtReportNumber.getAttribute("value");
			Log.message("Report number is: "+Report_Number_Auto);
			String Date_Auto = txtReportDate.getAttribute("value");
			Log.message("Auto selected date is: "+Date_Auto);
			SkySiteUtils.waitTill(1000);
			/*icon_ApproversDetails_Report.click();
			Log.message("Clicked on Approvers drop arrow.");
			SkySiteUtils.waitTill(5000);
			String Approver_Details = info_ApproverDetails.getText();
			Log.message("Approvers details are: "+Approver_Details);
			Approver_Details = info_ApproverDetails.getAttribute("value");
			Log.message("Approvers details are: "+Approver_Details);*/
			//Add Photos from computer
			btn_AddPhotos_Report.click();
			Log.message("Clicked on Add button for photos.");
			SkySiteUtils.waitTill(3000);
			icon_AddPhotos_Computer.click();
			Log.message("Clicked on from Computer icon.");
			SkySiteUtils.waitTill(10000);
			//Getting Photo path to be attach
			File PhotoPath_FMProperties = new File(PropertyReader.getProperty("SinglePhoto_TestData_Path"));
	   		String FolderPath_Photo = PhotoPath_FMProperties.getAbsolutePath().toString();
			
			// Writing File names into a text file for using in AutoIT Script
			BufferedWriter output;
			randomFileName rn = new randomFileName();
			String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
			output = new BufferedWriter(new FileWriter(tmpFileName, true));
			String expFilename = null;
			File[] files = new File(FolderPath_Photo).listFiles();
			for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();//Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
				}
			}
			output.flush();
			output.close();
			// Executing .exe autoIt file
			String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
			Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath_Photo + " " + tmpFileName);
			Log.message("AutoIT Script Executed!!");
			SkySiteUtils.waitTill(10000);			
			SkySiteUtils.waitForElement(driver, Attached_Photos_Info, 30);
			//Delete the temp file
			try
			{
				File file = new File(tmpFileName);
				if(file.delete())
				{
					Log.message(file.getName() + " is deleted!");
				}
				else
				{
					Log.message("Delete operation is failed.");
				}
			}
			catch(Exception e)
			{
				Log.message("Exception occured!!!"+e);	
			}
			SkySiteUtils.waitTill(3000);
			btn_LinkedFiles_Report.click();
			Log.message("Clicked on Add button for Link Files.");
			SkySiteUtils.waitTill(3000);
			icon_LinkFiles_Computer.click();
			Log.message("Clicked on from Computer icon.");
			SkySiteUtils.waitTill(10000);
			//Getting linked file path to be attach
			File Path_ReplaceFile = new File(PropertyReader.getProperty("Replace_FilePath"));
	   		String PDF_Replace_FoldPath = Path_ReplaceFile.getAbsolutePath().toString();
			//Writing File names into a text file for using in AutoIT Script
			BufferedWriter output1;
			randomFileName rn1 = new randomFileName();
			String tmpFileName1 = "./AutoIT_TempFile/" + rn1.nextFileName() + ".txt";
			output1 = new BufferedWriter(new FileWriter(tmpFileName1, true));
			String expFilename1 = null;
			File[] files_Photo = new File(PDF_Replace_FoldPath).listFiles();
			for (File file : files_Photo) {
			if (file.isFile()) {
				expFilename1 = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename1);
				output1.append('"' + expFilename1 + '"');
				output1.append(" ");
				SkySiteUtils.waitTill(1000);
				}
			}
			output1.flush();
			output1.close();
			// Executing .exe autoIt file
			//String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
			Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + PDF_Replace_FoldPath + " " + tmpFileName1);
			Log.message("AutoIT Script Executed!!");
			SkySiteUtils.waitTill(10000);			
			SkySiteUtils.waitForElement(driver, Attached_Files_Info, 30);
			//Delete the temp file
			try
			{
				File file = new File(tmpFileName);
				if(file.delete())
				{
					Log.message(file.getName() + " is deleted!");
				}
				else
				{
					Log.message("Delete operation is failed.");
				}
			}
			catch(Exception e)
			{
				Log.message("Exception occured!!!"+e);	
			}
			/*txt_AddActivityComment.click();
			SkySiteUtils.waitTill(2000);
			txt_AddActivityComment.sendKeys("Going to Approve or Reject from approver side.");
			SkySiteUtils.waitTill(2000);
			btn_AddComment_Submit.click();
			Log.message("Entered activity comment.");
			SkySiteUtils.waitTill(2000);
			SkySiteUtils.waitForElement(driver, info_AddedCommentProof, 20);
			String AddedComment = info_AddedCommentProof.getText();
			Log.message("info after Added Comment is: "+AddedComment);*/
			SkySiteUtils.waitTill(2000);
			btnSubmitReport.click();
			Log.message("Clicked on Submit button.");
			SkySiteUtils.waitForElement(driver, btnYes, 20);
			btnYes.click();
			SkySiteUtils.waitTill(20000);
			SkySiteUtils.waitForElement(driver, status_Pending, 20);
			String Act_ReportNO_SubmittedList = ReportNO_approved.getText();
			Log.message("Report NO after submit is: "+Act_ReportNO_SubmittedList);
			if((Date_Auto!=null)&&(Report_Number_Auto.contentEquals(Exp_ReportNO)) && (Act_ReportNO_SubmittedList.contains(Exp_ReportNO))
					)//&& (Approver_Details.contains(Approver_Name))
			{
				result2 = true;
				Log.message("Report details are proper and status was Pending.");
			}
			else
			{
				result2 = false;
				Log.message("Report details are not proper.");
			}
		} 
		if((result1==true) && (result2==true))
		{
			Log.message("Approver successfully submit the report.");
			return true;
		}
		else
		{
			Log.message("User Unable to submit the report.");
			return false;
		}
	}
	
	/**
	 * Method written for Approve a report and validate.
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws AWTException 
	 */
	public boolean Approve_Report_Validate(String Randome_TemplateName,String Input_ApprovalType,String Approver_Name) throws IOException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		SkySiteUtils.waitForElement(driver, status_Pending, 20);
		status_Pending.click();
		Log.message("Clicked on Pending button to navigate to report details for Approve.");
		SkySiteUtils.waitForElement(driver, ReportStatus_WhileApproveORReject, 60);
		SkySiteUtils.waitTill(10000);
		String Current_Status = ReportStatus_WhileApproveORReject.getAttribute("value");
		Log.message("Current status is: "+Current_Status);
		Log.message("Clicked on Approvers drop arrow.");
		String Approver_Details = info_ApproverDetails.getText();
		Log.message("Approvers details are: "+Approver_Details);
		Approver_Details = info_ApproverDetails.getAttribute("value");
		Log.message("Approvers details are: "+Approver_Details);
		SkySiteUtils.waitForElement(driver, info_AddedCommentProof, 20);
		String AddedComment = info_AddedCommentProof.getText();
		Log.message("info for Added Comment is: "+AddedComment);
		SkySiteUtils.waitTill(2000);
		if ((Current_Status.contentEquals("Pending"))&&(Approver_Details.contains(Approver_Name))&&(AddedComment.contains(Approver_Name))
				&& (iconDownload_PhotoAttached.isDisplayed()) && (iconDownload_FileAttached.isDisplayed())) 
		{
			result1=true;
			Log.message("Report is having proper detailes. User can Approve.");
			btn_Approve.click();
			Log.message("Clicked on Approve button.");
			
			
			//Need to implement more validations - Approve and Reject buttons are working 
			SkySiteUtils.waitTill(15000); 
			SkySiteUtils.waitForElement(driver, status_approved, 20);
			String Act_ReportNO_SubmittedList = ReportNO_approved.getText();
			Log.message("Report NO after submit is: "+Act_ReportNO_SubmittedList);
			if(status_approved.isDisplayed())
			{
				result2 = true;
				Log.message("Approving a report is working.");
			}
			else
			{
				result2 = false;
				Log.message("Approving a report is NOT working.");
			}
		} 
		if((result1==true) && (result2==true))
		{
			Log.message("User successfully Approved a report.");
			return true;
		}
		else
		{
			Log.message("User Failed to Approved the report.");
			return false;
		}
	}
	
	/**
	 * Method written for Reject a report and validate.
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws AWTException 
	 */
	public boolean Reject_Report_Validate(String Randome_TemplateName,String Input_ApprovalType,String Approver_Name) throws IOException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		SkySiteUtils.waitForElement(driver, status_Pending, 20);
		status_Pending.click();
		Log.message("Clicked on Pending button to navigate to report details for Approve.");
		SkySiteUtils.waitForElement(driver, ReportStatus_WhileApproveORReject, 60);
		SkySiteUtils.waitTill(10000);
		String Current_Status = ReportStatus_WhileApproveORReject.getAttribute("value");
		Log.message("Current status is: "+Current_Status);
		Log.message("Clicked on Approvers drop arrow.");
		String Approver_Details = info_ApproverDetails.getText();
		Log.message("Approvers details are: "+Approver_Details);
		Approver_Details = info_ApproverDetails.getAttribute("value");
		Log.message("Approvers details are: "+Approver_Details);
		SkySiteUtils.waitForElement(driver, info_AddedCommentProof, 20);
		String AddedComment = info_AddedCommentProof.getText();
		Log.message("info for Added Comment is: "+AddedComment);
		SkySiteUtils.waitTill(2000);
		if ((Current_Status.contentEquals("Pending"))&&(Approver_Details.contains(Approver_Name))&&(AddedComment.contains(Approver_Name))
				&& (iconDownload_PhotoAttached.isDisplayed()) && (iconDownload_FileAttached.isDisplayed())) 
		{
			result1=true;
			Log.message("Report is having proper detailes. User can Reject.");
			btn_Reject.click();
			Log.message("Clicked on Reject button.");
		
			//Need to implement more validations - Approve and Reject buttons are working 
			SkySiteUtils.waitTill(15000); 
			SkySiteUtils.waitForElement(driver, status_approved, 20);
			String Act_ReportNO_SubmittedList = ReportNO_approved.getText();
			Log.message("Report NO after submit is: "+Act_ReportNO_SubmittedList);
			if(status_approved.isDisplayed())
			{
				result2 = true;
				Log.message("Reject a report is working.");
			}
			else
			{
				result2 = false;
				Log.message("Reject a report is NOT working.");
			}
		} 
		if((result1==true) && (result2==true))
		{
			Log.message("User successfully Rejected the report.");
			return true;
		}
		else
		{
			Log.message("User failed to Reject the report.");
			return false;
		}
	}
	
	/**
	 * Method written for Draft a report and validate.
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws AWTException 
	 */
	public boolean Draft_Report_Validate(String Randome_TemplateName,String Input_ApprovalType) throws IOException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		//Counting available list of templates
		int NoOf_Templates = driver.findElements(By.xpath("//h4[@class='media-heading' and @aria-label='TemplateName']")).size();
		Log.message("Available Templates are : "+NoOf_Templates);
		int i = 0;
		for(i=1;i<=NoOf_Templates;i++)
		{
			String Act_TempName = driver.findElement(By.xpath("(//h4[@class='media-heading' and @aria-label='TemplateName'])["+i+"]")).getText();
			Log.message("Template after create is: "+Act_TempName);
			String Act_ApprovalType = driver.findElement(By.xpath("(//span[@class='temp-approver'])["+i+"]")).getText();
			Log.message("Template Approval type is: "+Act_ApprovalType);
			if ((Act_TempName.trim().contentEquals(Randome_TemplateName)) && (Act_ApprovalType.trim().contentEquals(Input_ApprovalType))) 
			{
				Log.message("Template is available.");
				result1 = true;
				break;
			} 
		}
		
		if (result1 == true) 
		{
			Log.message("Expected Template is available. User can Create and Submit.");
			driver.findElement(By.xpath("(//button[@class='btn btn-primary' and @routerlink='/createReport'])["+i+"]")).click();
			Log.message("Clicked on Create button.");
			SkySiteUtils.waitForElement(driver, btnSubmitReport, 60);
			SkySiteUtils.waitTill(15000); 
			String Report_Number_Auto = txtReportNumber.getAttribute("value");
			Log.message("Report number is: "+Report_Number_Auto);
			String Date_Auto = txtReportDate.getAttribute("value");
			Log.message("Auto selected date is: "+Date_Auto);
			SkySiteUtils.waitTill(1000);
			btnDraftReport.click();
			Log.message("Clicked on Draft button.");
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, status_approved, 20);
			String Act_ReportNO_SubmittedList = ReportNO_approved.getText();
			Log.message("Report NO after submit is: "+Act_ReportNO_SubmittedList);
			if((Report_Number_Auto.contentEquals("1")) && (Act_ReportNO_SubmittedList.contains("1")))
			{
				result2 = true;
				Log.message("Report details are proper and listed under draft.");
			}
			else
			{
				result2 = false;
				Log.message("Report details are not proper.");
			}
		} 
		if((result1==true) && (result2==true))
		{
			Log.message("User successfully Draft the report.");
			return true;
		}
		else
		{
			Log.message("User Unable to Draft the report.");
			return false;
		}
	}
	
	/**
	 * Method written for Delete template and validate
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws AWTException 
	 */
	public boolean Delete_Template_AndValidate(String Randome_TemplateName,String Input_ApprovalType) throws IOException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		//Counting available list of templates
		int NoOf_Templates = driver.findElements(By.xpath("//h4[@class='media-heading' and @aria-label='TemplateName']")).size();
		Log.message("Available Templates are : "+NoOf_Templates);
		int i = 0;
		for(i=1;i<=NoOf_Templates;i++)
		{
			String Act_TempName = driver.findElement(By.xpath("(//h4[@class='media-heading' and @aria-label='TemplateName'])["+i+"]")).getText();
			Log.message("Template after create is: "+Act_TempName);
			String Act_ApprovalType = driver.findElement(By.xpath("(//span[@class='temp-approver'])["+i+"]")).getText();
			Log.message("Template Approval type is: "+Act_ApprovalType);
			if ((Act_TempName.trim().contentEquals(Randome_TemplateName)) && (Act_ApprovalType.trim().contentEquals(Input_ApprovalType))) 
			{
				Log.message("Template is available.");
				result1 = true;
				break;
			} 
		}
		
		if (result1 == true) 
		{
			Log.message("Expected Template is available. User can delete.");
			driver.findElement(By.xpath("(//span[@class='icon icon-pr-delete'])["+i+"]")).click();
			Log.message("Clicked on delete icon to delete the template.");
			SkySiteUtils.waitForElement(driver, btnYes, 20);
			btnYes.click();
			SkySiteUtils.waitTill(1000);
			SkySiteUtils.waitForElement(driver, infoNotifyMsg, 20);
			String infMesg_AfterDeleteTemplae = infoNotifyMsg.getText();
			Log.message("Message After delete a template is: "+infMesg_AfterDeleteTemplae);
			if(infMesg_AfterDeleteTemplae.contentEquals("Template deleted successfully."))
			{
				result2 = true;
				Log.message("Template deleted successfully.");
			}
			else
			{
				result2 = false;
				Log.message("Failed to delete Template.");
			}
		} 
		if((result1==true) && (result2==true))
		{
			Log.message("User successfully delted the Template.");
			return true;
		}
		else
		{
			Log.message("User Unable to delted the Template.");
			return false;
		}
	}
	
	/**
	 * Method written for Download template and validate
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws AWTException 
	 */
	@FindBy(xpath="(//a[@id='download-id']/span)[1]")
	WebElement iconDownloadTemplate;
	public boolean Download_Template_AndValidate(String Randome_TemplateName,String Input_ApprovalType,String Sys_Download_Path) throws IOException, AWTException, InterruptedException 
	{
		boolean result1 = false;
		boolean result2 = false;
		//Counting available list of templates
		int NoOf_Templates = driver.findElements(By.xpath("//h4[@class='media-heading' and @aria-label='TemplateName']")).size();
		Log.message("Available Templates are : "+NoOf_Templates);
		int i = 0;
		for(i=1;i<=NoOf_Templates;i++)
		{
			String Act_TempName = driver.findElement(By.xpath("(//h4[@class='media-heading' and @aria-label='TemplateName'])["+i+"]")).getText();
			Log.message("Template after create is: "+Act_TempName);
			String Act_ApprovalType = driver.findElement(By.xpath("(//span[@class='temp-approver'])["+i+"]")).getText();
			Log.message("Template Approval type is: "+Act_ApprovalType);
			if ((Act_TempName.trim().contentEquals(Randome_TemplateName)) && (Act_ApprovalType.trim().contentEquals(Input_ApprovalType))) 
			{
				Log.message("Template is available.");
				result1 = true;
				break;
			} 
		}
		
		if (result1 == true) 
		{
			Log.message("Expected Template is available. User can download.");
			// Calling "Deleting download folder files" method
			projectDashboardPage = new ProjectDashboardPage(driver).get();
			projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
			SkySiteUtils.waitTill(5000);
			driver.findElement(By.xpath("(//a[@id='download-id']/span)["+i+"]")).click();
			Log.message("Clicked on download icon.");
			SkySiteUtils.waitTill(20000);
			// Get Browser name on run-time.
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			Log.message("Browser name on run-time is: " + browserName);
			if (browserName.contains("firefox")) 
			{
				// Handling Download PopUp of firefox browser using robot
				Robot robot = null;
				robot = new Robot();

				robot.keyPress(KeyEvent.VK_ALT);
				SkySiteUtils.waitTill(2000);
				robot.keyPress(KeyEvent.VK_S);
				SkySiteUtils.waitTill(2000);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_S);
				SkySiteUtils.waitTill(3000);
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				SkySiteUtils.waitTill(30000);
			}	
			//After Download, checking whether Downloaded file is existed under
			String ActualFilename = null;
			File[] files = new File(Sys_Download_Path).listFiles();

			for (File file : files) 
			{
				if (file.isFile()) 
				{
					ActualFilename = file.getName();// Getting File Names into a variable
					Log.message("Actual File name is:" + ActualFilename);
					Long ActualFileSize = file.length();
					Log.message("Actual File size is:" + ActualFileSize);
					if ((ActualFilename!=null) && (ActualFileSize != 0)) 
					{
						Log.message("Gallery Folder is available in downloads!!!");
						result2 = true;
						break;
					}
				}
			}				
		} 
		if((result1==true) && (result2==true))
		{
			Log.message("User successfully Downloaded the Template.");
			return true;
		}
		else
		{
			Log.message("User Unable to Downloaded the Template.");
			return false;
		}
	}
	
	/**
	 * Method written for Edit template and validate
	 * Scipted By: Naresh Babu
	 * @return
	 * @throws AWTException 
	 */
	public boolean Edit_Template_AndValidate(String Randome_TemplateName,String Input_ApprovalType,String FolderPath,String Edit_TempName) throws IOException, AWTException 
	{
		boolean result1 = false;
		boolean result2 = false;
		//Counting available list of templates
		int NoOf_Templates = driver.findElements(By.xpath("//h4[@class='media-heading' and @aria-label='TemplateName']")).size();
		Log.message("Available Templates are : "+NoOf_Templates);
		int i = 0;
		for(i=1;i<=NoOf_Templates;i++)
		{
			String Act_TempName = driver.findElement(By.xpath("(//h4[@class='media-heading' and @aria-label='TemplateName'])["+i+"]")).getText();
			Log.message("Template after create is: "+Act_TempName);
			String Act_ApprovalType = driver.findElement(By.xpath("(//span[@class='temp-approver'])["+i+"]")).getText();
			Log.message("Template Approval type is: "+Act_ApprovalType);
			if ((Act_TempName.trim().contentEquals(Randome_TemplateName)) && (Act_ApprovalType.trim().contentEquals(Input_ApprovalType))) 
			{
				Log.message("Template is available.");
				result1 = true;
				break;
			} 
		}
		
		if (result1 == true) 
		{
			Log.message("Expected Template is available. User can Edit.");
			driver.findElement(By.xpath("(//span[@class='icon icon-pr-edit'])["+i+"]")).click();
			Log.message("Clicked on Edit icon to Edit the template.");
			SkySiteUtils.waitForElement(driver, btnReplaceFileEditTemp, 30);
			SkySiteUtils.waitTill(5000);
			btnReplaceFileEditTemp.click();
			Log.message("Clicked on Replace a file button");
			SkySiteUtils.waitTill(10000);
			// Writing File names into a text file for using in AutoIT Script
			BufferedWriter output;
			randomFileName rn = new randomFileName();
			String tmpFileName = "./AutoIT_TempFile/" + rn.nextFileName() + ".txt";
			output = new BufferedWriter(new FileWriter(tmpFileName, true));
			String expFilename = null;
			File[] files = new File(FolderPath).listFiles();
			for (File file : files) {
			if (file.isFile()) {
				expFilename = file.getName();// Getting File Names into a variable
				Log.message("Expected File name is:" + expFilename);
				output.append('"' + expFilename + '"');
				output.append(" ");
				SkySiteUtils.waitTill(1000);
				}
			}
			output.flush();
			output.close();
			// Executing .exe autoIt file
			String AutoIt_ExeFile_Path = PropertyReader.getProperty("AutoItExe_FilePath");
			Runtime.getRuntime().exec(AutoIt_ExeFile_Path + " " + FolderPath + " " + tmpFileName);
			Log.message("AutoIT Script Executed!!");
			SkySiteUtils.waitTill(10000);			
			SkySiteUtils.waitForElement(driver, infoAttachedPdf, 30);
			//Delete the temp file
			try
			{
				File file = new File(tmpFileName);
				if(file.delete())
				{
					Log.message(file.getName() + " is deleted!");
				}
				else
				{
					Log.message("Delete operation is failed.");
				}
			}
			catch(Exception e)
			{
				Log.message("Exception occured!!!"+e);	
			}
			String info_AftrReplaceFile = infoTemplateName_Replace.getText();
			Log.message("Template name after Replace a PDF is : "+info_AftrReplaceFile);
			txtTemplateName_BeforeEnter.clear();
			SkySiteUtils.waitTill(3000);
			txtTemplateName_Edit.click();
			Log.message("Clicked on input field.");
			txtTemplateName_Edit.sendKeys(Edit_TempName);
			Log.message("Template name is editing as : "+Edit_TempName);
			SkySiteUtils.waitTill(2000);
			btnSaveUploadTemplate.click();
			Log.message("Clicked on Save button after Edit all details.");
			SkySiteUtils.waitTill(30000);
			SkySiteUtils.waitForElement(driver, TemplateName_List, 30);
			for(i=1;i<=NoOf_Templates;i++)
			{
				String Act_TempName = driver.findElement(By.xpath("(//h4[@class='media-heading' and @aria-label='TemplateName'])["+i+"]")).getText();
				Log.message("Template after Edit is: "+Act_TempName);
				String Act_ApprovalType = driver.findElement(By.xpath("(//span[@class='temp-approver'])["+i+"]")).getText();
				Log.message("Template Approval type is: "+Act_ApprovalType);
				if ((Act_TempName.trim().contentEquals(Edit_TempName)) && (Act_ApprovalType.trim().contentEquals(Input_ApprovalType))) 
				{
					Log.message("Edited Template is available.");
					result2 = true;
					break;
				} 
			}
		} 
		if((result1==true) && (result2==true))
		{
			Log.message("User successfully Editing the Template.");
			return true;
		}
		else
		{
			Log.message("User Failed to Edit the Template.");
			return false;
		}
	}
	
/** Method written for Export templates and validate
 * Scipted By: Naresh Babu
 * @return
 * @throws AWTException 
 * @throws InterruptedException 
 */
	@FindBy(xpath="//a[@aria-label='export']")
	WebElement btn_Export;
public boolean Export_Templates_AndValidate(String Randome_TemplateName,String Input_ApprovalType,String Sys_Download_Path,String Reporter_Name) throws IOException, AWTException, InterruptedException 
{
	boolean result1 = false;
	boolean result2 = false;
	//Counting available list of templates
	int NoOf_Templates = driver.findElements(By.xpath("//h4[@class='media-heading' and @aria-label='TemplateName']")).size();
	Log.message("Available Templates are : "+NoOf_Templates);
	if (NoOf_Templates>=1) 
	{
		Log.message("Templates are available. User can Export.");
		//Calling "Deleting download folder files" method
		projectDashboardPage = new ProjectDashboardPage(driver).get();
		projectDashboardPage.Delete_ExistedFiles_From_DownloadFolder(Sys_Download_Path);
		SkySiteUtils.waitTill(5000);
		btn_Export.click();
		Log.message("Clicked on Export button.");
		SkySiteUtils.waitTill(25000);
		// Get Browser name on run-time.
		Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = caps.getBrowserName();
		Log.message("Browser name on run-time is: " + browserName);
		if (browserName.contains("firefox")) 
		{
			// Handling Download PopUp of firefox browser using robot
			Robot robot = null;
			robot = new Robot();

			robot.keyPress(KeyEvent.VK_ALT);
			SkySiteUtils.waitTill(2000);
			robot.keyPress(KeyEvent.VK_S);
			SkySiteUtils.waitTill(2000);
			robot.keyRelease(KeyEvent.VK_ALT);
			robot.keyRelease(KeyEvent.VK_S);
			SkySiteUtils.waitTill(3000);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			SkySiteUtils.waitTill(30000);
		}	
		//After Export, checking whether file is available under downloads or not
		String ActualFilename = null;
		File[] files = new File(Sys_Download_Path).listFiles();

		for (File file : files) 
		{
			if (file.isFile()) 
			{
				ActualFilename = file.getName();// Getting File Names into a variable
				Log.message("Actual File name is:" + ActualFilename);
				Long ActualFileSize = file.length();
				Log.message("Actual File size is:" + ActualFileSize);
				if ((ActualFilename.contains("Template_export_")) && (ActualFileSize != 0)) 
				{
					Log.message("File is available in downloads!!!");
					result1 = true;
					break;
				}
			}
		}
		String usernamedir=System.getProperty("user.name");
  		String XLSXFileToRead = "C:\\" + "Users\\" + usernamedir + "\\Downloads\\"+ActualFilename;
  		Log.message("Reading values from XLSX file is: "+XLSXFileToRead);
  		//Reading data from XLSX file
  		File XLSXfile = new File(XLSXFileToRead);//Create an object of File class to open xlsx file
  		FileInputStream inputStream = new FileInputStream(XLSXfile);//Create an object of FileInputStream class to read excel file
  		XSSFWorkbook my_xls_workbook = new XSSFWorkbook(inputStream);//Access the workbook
  		XSSFSheet my_worksheet = my_xls_workbook.getSheetAt(0);//Access the work sheet
		int RowCount_HaveData = my_worksheet.getLastRowNum()-my_worksheet.getFirstRowNum();//Getting row count 
		Log.message("Row count is: "+RowCount_HaveData);
		int i = 0;
		int SuccessCount = 0;
		for(i=1;i<=RowCount_HaveData;i++)
		{
			String Template_Name = my_worksheet.getRow(i).getCell(0).getStringCellValue();
			Log.message("Actual Template Name is: "+Template_Name);
			String Approval_Type = my_worksheet.getRow(i).getCell(2).getStringCellValue();
			Log.message("Actual Approval Type is: "+Approval_Type);
			//String Report_Count = my_worksheet.getRow(i).getCell(6).getStringCellValue();
			//String Report_Count = NumberToTextConverter.toText(my_worksheet.getRow(i).getCell(2).getNumericCellValue());
			//Log.message("Actual Report count is: "+Report_Count);
			String Creater_Name = my_worksheet.getRow(i).getCell(7).getStringCellValue();
			Log.message("Actual Creater name is: "+Creater_Name);
			if((Template_Name.contentEquals(Randome_TemplateName)) && (Approval_Type.contentEquals(Input_ApprovalType))
					&& (Creater_Name.contains(Reporter_Name)))//&& (Report_Count.contentEquals("0")) 
			{
				SuccessCount=SuccessCount+1;
				Log.message("Success count is: "+SuccessCount);
			}
		}
		inputStream.close();//Close the stream
		if((SuccessCount==1))// && (RowCount_HaveData==NoOf_Templates)
		{
			result2 = true;
			Log.message("Expected NO OF recards are available with data");
		}
	} 
	if((result1==true) && (result2==true))
	{
		Log.message("Template Export is working.");
		return true;
	}
	else
	{
		Log.message("Template Export is NOT working.");
		return false;
	}
}

/**
 * Method written for Try to edit approved report and validate.
 * Scipted By: Naresh Babu
 * @return
 * @throws AWTException 
 */
public boolean Verify_AprvedReport_AbleToEdit(String TemplateName,String Input_ApprovalType,String CreatorName) throws IOException, AWTException 
{
	boolean result1 = false;
	boolean result2 = false;
	//Counting available list of templates
	int NoOf_Templates = driver.findElements(By.xpath("//h4[@class='media-heading' and @aria-label='TemplateName']")).size();
	Log.message("Available Templates are : "+NoOf_Templates);
	int i = 0;
	for(i=1;i<=NoOf_Templates;i++)
	{
		String App_Creator_Name = driver.findElement(By.xpath("(//span[@class='text-dark'])[2]")).getText();
		Log.message("Creater name is: "+App_Creator_Name);
		String Act_TempName = driver.findElement(By.xpath("(//h4[@class='media-heading' and @aria-label='TemplateName'])["+i+"]")).getText();
		Log.message("Template after create is: "+Act_TempName);
		String Act_ApprovalType = driver.findElement(By.xpath("(//span[@class='temp-approver'])["+i+"]")).getText();
		Log.message("Template Approval type is: "+Act_ApprovalType);
		if ((App_Creator_Name.contains(CreatorName)) && (Act_TempName.trim().contentEquals(TemplateName)) 
				&& (Act_ApprovalType.trim().contentEquals(Input_ApprovalType))) 
		{
			Log.message("Template is available with expected details.");
			result1 = true;
			break;
		} 
	}
	
	if (result1 == true) 
	{
		Log.message("Expected Template is available. User can Create and Submit.");
	}
	if((result1==true) && (result2==true))
	{
		Log.message("User not able to do any modification on Approved report.");
		return true;
	}
	else
	{
		Log.message("User is able to do modifications on approved report.");
		return false;
	}
	
	
}
	
}
