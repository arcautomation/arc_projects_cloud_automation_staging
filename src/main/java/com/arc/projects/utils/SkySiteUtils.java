package com.arc.projects.utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.google.common.base.Function;

import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.StopWatch;

/**
 * These are reusable methods can be used across SkySite Automation project.
 */

public class SkySiteUtils{
	static WebDriver driver ;
	
	public static boolean waitForElement(WebDriver driver, WebElement element, int maxWait)
	{
		boolean statusOfElementToBeReturned = false;
        long startTime = StopWatch.startTime();
        WebDriverWait wait = new WebDriverWait(driver, maxWait);
        try {
            WebElement waitElement = wait.until(ExpectedConditions.visibilityOf(element));
            if (waitElement.isDisplayed() && waitElement.isEnabled()) {
                statusOfElementToBeReturned = true;
                Log.event("Element is displayed:: " + element.toString());
            }
        } catch (Exception e) {
            try {
				throw new Exception("Unable to find a element after " + StopWatch.elapsedTime(startTime) + " sec ==> " + element.toString());
			} catch (Exception e1) {
				e1.printStackTrace();
			}
        }
        return statusOfElementToBeReturned;
    }
	
	public static void waitTill(int value){
		   try {
			     Thread.sleep(value);
		    } catch (InterruptedException e) {
			     e.printStackTrace();
		      }
	   }
	
	
	
	
	
	
	public static boolean FluentWait(WebElement element, int timelimit, int interval) throws InterruptedException{
		
		boolean result = false;
		
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
			    .withTimeout(timelimit, TimeUnit.SECONDS)
			    .pollingEvery(interval, TimeUnit.SECONDS)
			    .ignoring(NoSuchElementException.class);

			WebElement foo = wait.until(new Function<WebDriver, WebElement>() 
			{
			  public WebElement apply(WebDriver driver) {
			  return element;
			}
			});
			return result;
	    
	}
	
	
	public static boolean waitForElementLoadTime(WebDriver driver, WebElement element, int maxWait)
    {
        boolean statusOfElementToBeReturned = false;
        long startTime = StopWatch.startTime();
        WebDriverWait wait = new WebDriverWait(driver, maxWait);
        try {
            WebElement waitElement = wait.until(ExpectedConditions.visibilityOf(element));
            if (waitElement.isDisplayed() && waitElement.isEnabled()) {
                statusOfElementToBeReturned = true;
                Log.event("Element is displayed: " + element.toString());
Log.message(element.toString() + "is displayed after: " + StopWatch.elapsedTime(startTime) + " seconds" );

            }
        } 
       catch (Exception ex) {
                                                statusOfElementToBeReturned = false;
                                                Log.event("Unable to find an element after " + StopWatch.elapsedTime(startTime) + " sec" );
                                                Log.message(element.toString() + "Is not displayed after: " + StopWatch.elapsedTime(startTime) + " seconds" );
                                }
                                return statusOfElementToBeReturned;
    }
	
	
	
	public static boolean waitForElementPresent(WebElement element)
			throws Throwable {
		boolean flag = false;
		try {
			for (int i = 0; i < 500; i++) {
				if (element.isDisplayed()&& element.isEnabled()) {
					flag = true;
					return true;

				} else {
					Thread.sleep(100);

				}
			}

		} catch (Exception e) {

			Assert.assertTrue(flag,"Wait For requested Element Present : Falied to locate element ");

			e.printStackTrace();

			return false;
		} 

		return flag;

	}


	}
	

    
