package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectAndFolder_Level_Search;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arcautoframe.utils.Log;

public class Viewerannotation_TestCases {

	public static WebDriver driver;

	ProjectsLoginPage projectsLoginPage;
	ProjectDashboardPage projectDashboardPage;
	ViewerScreenPage viewerScreenPage;
	FolderPage folderPage;
	ProjectAndFolder_Level_Search projectAndFolder_Level_Search;

	@Parameters("browser")
	@BeforeMethod
	public WebDriver beforeTest(String browser) {

		if (browser.equalsIgnoreCase("firefox")) {
			File dest = new File("./drivers/win/geckodriver.exe");
			// System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
			System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
			driver = new FirefoxDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}

		else if (browser.equalsIgnoreCase("chrome")) {
			File dest = new File("./drivers/win/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put("download.default_directory", "C:" + File.separator + "Users" + File.separator
					+ System.getProperty("user.name") + File.separator + "Downloads");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setExperimentalOption("prefs", prefs);
			driver = new ChromeDriver(options);
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));

		}

		else if (browser.equalsIgnoreCase("safari")) {
			System.setProperty("webdriver.safari.noinstall", "true"); // To stop uninstall each time
			driver = new SafariDriver();
			driver.get(PropertyReader.getProperty("SkysiteProdURL"));
		}
		return driver;
	}

	/**
	 * TC_002 (Viewer annotation level cases): Verify the Line annotation drawn on the viewer getting saved properly.
	 * 
	 * @throws Exception
	 * Created By: Trinanjwan Sarkar
	 */

	@Test(priority = 0, enabled = true, description = "TC_002=Verify the Line annotation drawn on the viewer getting saved properly")
	public void verifysavingoflineannotation() throws Exception {
		try {
			Log.testCaseInfo("TC_002 (Viewer annotation level cases): Verify the Line annotation drawn on the viewer getting saved properly.");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#lineToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-line"));

			Log.assertThat(viewerScreenPage.drawAnnotation(markupname, filepath1,parentlocator, childlocator), "The line annotation is validated properly inside the viewer", "The line annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_081 (Viewer annotation level cases): Verify the freehand annotation drawn on the viewer getting saved properly.
	 * 
	 * @throws Exception
	 * Created by Trinanjwan Sarkar
	 */

	@Test(priority = 1, enabled = true, description = "TC_081=Verify the freehand annotation drawn on the viewer getting saved properly")
	public void verifysavingoffreehandannotation() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_081 (Viewer annotation level cases): Verify the freehand annotation drawn on the viewer getting saved properly.");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#lineToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-pen"));

			Log.assertThat(viewerScreenPage.drawAnnotation(markupname, filepath1, parentlocator, childlocator), "The freehand annotation is validated properly inside the viewer", "The freehand annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_080 (Viewer annotation level cases): Verify the arrow annotation drawn on the viewer getting saved properly.
	 * 
	 * @throws Exception
	 * Created by Trinanjwan Sarkar
	 */

	@Test(priority = 2, enabled = true, description = "TC_080=Verify the arrow annotation drawn on the viewer getting saved properly")
	public void verifysavingofarrowannotation() throws Exception {
		try {
			Log.testCaseInfo("TC_080 (Viewer annotation level cases): Verify the arrow annotation drawn on the viewer getting saved properly.");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#lineToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-arrow"));

		    Log.assertThat(viewerScreenPage.drawAnnotationarrow(markupname, filepath1, parentlocator, childlocator), "The arrow annotation is validated properly inside the viewer", "The arrow annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_001 (Viewer annotation level cases): Verify the Rectangle annotation drawn on the viewer getting saved properly.
	 * 
	 * @throws Exception
	 * Created by Trinanjwan Sarkar
	 */

	@Test(priority = 3, enabled = true, description = "TC_001=Verify the Rectangle annotation drawn on the viewer getting saved properly")
	public void verifysavingofrectangleannotation() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_001 (Viewer annotation level cases): Verify the Rectangle annotation drawn on the viewer getting saved properly.");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#shapeToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-rectangle"));

			Log.assertThat(viewerScreenPage.drawAnnotation(markupname, filepath1, parentlocator, childlocator), "The Rectangle annotation is validated properly inside the viewer", "The Rectangle annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_078 (Viewer annotation level cases): Verify the ellipse/circle annotation drawn on the viewer getting saved properly.
	 * 
	 * @throws Exception
	 * Created by Trinanjwan Sarkar
	 */

	@Test(priority = 4, enabled = true, description = "TC_078=Verify the ellipse/circle annotation drawn on the viewer getting saved properly")
	public void verifysavingofellipseannotation() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_078 (Viewer annotation level cases): Verify the ellipse/circle annotation drawn on the viewer getting saved properly.");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#shapeToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-ellipse"));

			 Log.assertThat(viewerScreenPage.drawAnnotation(markupname, filepath1, parentlocator, childlocator), "The ellipse/circle annotation is validated properly inside the viewer", "The ellipse/circle annotation validation got failed", driver);
			 viewerScreenPage.deletemarkup(markupname);
			 viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_079 (Viewer annotation level cases): Verify the cloud annotation drawn on the viewer getting saved properly.
	 * 
	 * @throws Exception
	 *  Created by Trinanjwan Sarkar
	 */

	@Test(priority = 5, enabled = true, description = "TC_079=Verify the cloud annotation drawn on the viewer getting saved properly")
	public void verifysavingofcloudannotation() throws Exception {
		try {
			Log.testCaseInfo("TC_079 (Viewer annotation level cases): Verify the cloud annotation drawn on the viewer getting saved properly.");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#shapeToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-cloud"));

			Log.assertThat(viewerScreenPage.drawAnnotation(markupname, filepath1, parentlocator, childlocator), "The cloud annotation is validated properly inside the viewer","The cloud annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	
	/**
	 * TC_082 (Viewer annotation level cases): Verify the Rectangle highlight annotation drawn on the viewer getting saved properly
	 * 
	 * @throws Exception
	 *  Created by Trinanjwan Sarkar
	 */

	@Test(priority = 6, enabled = true, description = "TC_082: Verify the Rectangle highlight annotation drawn on the viewer getting saved properly")
	public void verifysavingofrecthighlight() throws Exception {
		try {
			Log.testCaseInfo("TC_082 (Viewer annotation level cases): Verify the Rectangle highlight annotation drawn on the viewer getting saved properly");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			Log.assertThat(viewerScreenPage.drawAnnotationrecthigh(markupname, filepath1), "The Rectangle highlight annotation is validated properly inside the viewer","The Rectangle highlight annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}
	
	

	/**
	 * TC_004 (Viewer annotation level cases): Verify the text annotation drawn on the viewer getting saved properly
	 * 
	 * @throws Exception
	 * Created by Trinanjwan Sarkar
	 */

	@Test(priority = 7, enabled = true, description = "TC_004 (Viewer annotation level cases): Verify the text annotation drawn on the viewer getting saved properly")
	public void verifysavingtextannotation() throws Exception {
		try {
			Log.testCaseInfo("TC_004 (Viewer annotation level cases): Verify the text annotation drawn on the viewer getting saved properly");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			
			WebElement parentlocator = driver.findElement(By.cssSelector("#textToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-text"));
			Log.assertThat(viewerScreenPage.drawAnnotationtext(markupname, filepath1,parentlocator,childlocator),
					"The text annotation is validated properly inside the viewer", "The text annotation validation got failed",
					driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	

	/**
	 * TC_084 (Viewer annotation level cases): Verify the call out annotation drawn on the viewer getting saved properly
	 * 
	 * @throws Exception
	 * Created by Trinanjwan Sarkar
	 */

	@Test(priority = 8, enabled = true, description = "TC_084 (Viewer annotation level cases): Verify the call out annotation drawn on the viewer getting saved properly")
	public void verifysavingcalloutannotation() throws Exception {
		try {
			Log.testCaseInfo("TC_084 (Viewer annotation level cases): Verify the call out annotation drawn on the viewer getting saved properly");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			
			WebElement parentlocator = driver.findElement(By.cssSelector("#textToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-callout"));
			Log.assertThat(viewerScreenPage.drawAnnotationtext(markupname, filepath1,parentlocator,childlocator),
					"The call out annotation is validated properly inside the viewer", "The call out annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	
	/**
	 * TC_083 (Viewer annotation level cases): Verify the note annotation drawn on the viewer getting saved properly
	 * 
	 * @throws Exception
	 * Created by Trinanjwan Sarkar 
	 */

	@Test(priority = 9, enabled = true, description = "TC_083 (Viewer annotation level cases): Verify the note annotation drawn on the viewer getting saved properly")
	public void verifysavingnoteannotation() throws Exception {
		try {
			Log.testCaseInfo("TC_083 (Viewer annotation level cases): Verify the note annotation drawn on the viewer getting saved properly");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
	
			Log.assertThat(viewerScreenPage.drawAnnotationnote(markupname, filepath1),
					"The note annotation is validated properly inside the viewer", "The note annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	/**
	 * TC_005 (Viewer annotation level cases): Verify deletion of saved Markup Verification
	 * 
	 * @throws Exception
	 * Created By: Trinanjwan Sarkar
	 */

	@Test(priority = 10, enabled = true, description = "TC_005 (Viewer annotation level cases): Verify deletion of saved Markup Verification")
	public void verifydeletionofmarkup() throws Exception {
		try {
			Log.testCaseInfo("TC_005 (Viewer annotation level cases): Verify deletion of saved Markup Verification");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#lineToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-line"));
			
			

			Log.assertThat(viewerScreenPage.drawAnnotation(markupname, filepath1,parentlocator, childlocator), "The line annotation is validated properly inside the viewer", "The line annotation validation got failed", driver);
			Log.assertThat(viewerScreenPage.deletemarkupvalidation(markupname, filepath1), "The mark up is deleted successfully","The mark up is not deleted successfully");
			viewerScreenPage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	/**
	 * TC_011 (Viewer annotation level cases): Verify Hyperlink (Same folder, different file)
	 * 
	 * @throws Exception
	 * Created By: Trinanjwan Sarkar
	 */

	@Test(priority = 11, enabled = true, description = "TC_011 (Viewer annotation level cases): Verify Hyperlink (Same folder, different file)")
	public void hypsamefolderdifffile() throws Exception {
		try {
			Log.testCaseInfo("TC_011 (Viewer annotation level cases): Verify Hyperlink (Same folder, different file)");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enteringanyProject("Hyperlink Project");
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingsourcehyperlink();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			Log.assertThat(viewerScreenPage.hyperlink_samefolderdiffifle(markupname, filepath1), "Same folder, different file hyperlink is validated properly inside the viewer", "Same folder, different file hyperlink validation got failed", driver);
			
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	/**
	 * TC_012 (Viewer annotation level cases): Verify Hyperlink (Different folder, different file)
	 * 
	 * @throws Exception
	 * Created By: Trinanjwan Sarkar
	 */

	@Test(priority = 12, enabled = true, description = "TC_012 (Viewer annotation level cases): Verify Hyperlink (Different folder, different file)")
	public void hypdifffolderdifffile() throws Exception {
		try {
			Log.testCaseInfo("TC_012 (Viewer annotation level cases): Verify Hyperlink (Different folder, different file)");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enteringanyProject("Hyperlink Project");
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingsourcehyperlink();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			Log.assertThat(viewerScreenPage.hyperlink_difffolderdiffifle(markupname, filepath1), "Different folder, different file hyperlink is validated properly inside the viewer", "Different folder, different file hyperlink validation got failed", driver);
			
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	/**
	 * TC_019 (Viewer annotation level cases): Verify Hyperlink (With Different folder (Parent -> Child) & Different file)
	 * 
	 * @throws Exception
	 * Created By: Trinanjwan Sarkar
	 */

	@Test(priority = 13, enabled = true, description = "TC_019 (Viewer annotation level cases): Verify Hyperlink (With Different folder (Parent -> Child) & Different file)")
	public void hypdifffolderdifffileparent2child() throws Exception {
		try {
			Log.testCaseInfo("TC_019 (Viewer annotation level cases): Verify Hyperlink (With Different folder (Parent -> Child) & Different file)");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enteringanyProject("Hyperlink Project");
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingsourcehyperlinkparent();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			Log.assertThat(viewerScreenPage.hyperlink_difffolderdiffifleparenttochild(markupname, filepath1), "Different folder (Parent -> Child) & Different file hyperlink is validated properly inside the viewer", "Different folder (Parent -> Child) & Different file hyperlink validation got failed", driver);
			
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	
	/**
	 * TC_020 (Viewer annotation level cases): Verify Hyperlink (With Different folder (Child -> Parent) & Different file)
	 * 
	 * @throws Exception
	 * Created By: Trinanjwan Sarkar
	 */

	@Test(priority = 14, enabled = true, description = "TC_020 (Viewer annotation level cases): Verify Hyperlink (With Different folder (Child -> Parent) & Different file)")
	public void hypdifffolderdifffilechild2parent() throws Exception {
		try {
			Log.testCaseInfo("TC_020 (Viewer annotation level cases): Verify Hyperlink (With Different folder (Child -> Parent) & Different file)");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enteringanyProject("Hyperlink Project");
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingsourcehyperlinkchild();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();
			Log.assertThat(viewerScreenPage.hyperlink_difffolderdiffiflechildtoparent(markupname, filepath1), "Different folder (Child -> Parent) & Different file hyperlink is validated properly inside the viewer", "Different folder (Child -> Parent) & Different file hyperlink validation got failed", driver);
			
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	/**
	 * TC_093 (Viewer annotation level cases): Verify the Line annotation drawn on the viewer getting saved properly after zoom in the document.
	 * 
	 * @throws Exception
	 * Created By: Trinanjwan Sarkar
	 */

	@Test(priority = 15, enabled = true, description = "TC_093 (Viewer annotation level cases): Verify the Line annotation drawn on the viewer getting saved properly after zoom in the document.")
	public void verifysavingoflineannotationzoomin() throws Exception {
		try {
			Log.testCaseInfo("TC_093 (Viewer annotation level cases): Verify the Line annotation drawn on the viewer getting saved properly after zoom in the document.");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#lineToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-line"));

			Log.assertThat(viewerScreenPage.drawAnnotationzoomin(markupname, filepath1,parentlocator, childlocator), "The line annotation is validated properly inside the viewer", "The line annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	
	/**
	 * TC_096 (Viewer annotation level cases): Verify the freehand annotation drawn on the viewer getting saved properly after zoom in the document.
	 * 
	 * @throws Exception
	 * Created by Trinanjwan Sarkar
	 */

	@Test(priority = 16, enabled = true, description = "TC_096 (Viewer annotation level cases): Verify the freehand annotation drawn on the viewer getting saved properly after zoom in the document.")
	public void verifysavingoffreehandannotationzoomin() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_096 (Viewer annotation level cases): Verify the freehand annotation drawn on the viewer getting saved properly after zoom in the document.");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#lineToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-pen"));

			Log.assertThat(viewerScreenPage.drawAnnotationzoomin(markupname, filepath1, parentlocator, childlocator), "The freehand annotation is validated properly inside the viewer", "The freehand annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	/**
	 * TC_090 (Viewer annotation level cases): Verify the Rectangle annotation drawn on the viewer getting saved properly after zoom in the document.
	 * 
	 * @throws Exception
	 * Created by Trinanjwan Sarkar
	 */

	@Test(priority = 17, enabled = true, description = "TC_090 (Viewer annotation level cases): Verify the Rectangle annotation drawn on the viewer getting saved properly after zoom in the document.")
	public void verifysavingofrectangleannotationzoomin() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_090 (Viewer annotation level cases): Verify the Rectangle annotation drawn on the viewer getting saved properly after zoom in the document.");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#shapeToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-rectangle"));

			Log.assertThat(viewerScreenPage.drawAnnotationzoomin(markupname, filepath1, parentlocator, childlocator), "The Rectangle annotation is validated properly inside the viewer", "The Rectangle annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}	
	
	/**
	 * TC_091 (Viewer annotation level cases): Verify the ellipse/circle annotation drawn on the viewer getting saved properly after zoom in the document.
	 * 
	 * @throws Exception
	 * Created by Trinanjwan Sarkar
	 */

	@Test(priority = 18, enabled = true, description = "TC_091 (Viewer annotation level cases): Verify the ellipse/circle annotation drawn on the viewer getting saved properly after zoom in the document.")
	public void verifysavingofellipseannotationzoom() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_091 (Viewer annotation level cases): Verify the ellipse/circle annotation drawn on the viewer getting saved properly after zoom in the document.");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#shapeToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-ellipse"));

			 Log.assertThat(viewerScreenPage.drawAnnotationzoomin(markupname, filepath1, parentlocator, childlocator), "The ellipse/circle annotation is validated properly inside the viewer", "The ellipse/circle annotation validation got failed", driver);
			 viewerScreenPage.deletemarkup(markupname);
			 viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	
	/**
	 * TC_092 (Viewer annotation level cases): Verify the cloud annotation drawn on the viewer getting saved properly after zoom in the document.
	 * 
	 * @throws Exception
	 *  Created by Trinanjwan Sarkar
	 */

	@Test(priority = 19, enabled = true, description = "TC_092 (Viewer annotation level cases): Verify the cloud annotation drawn on the viewer getting saved properly after zoom in the document.")
	public void verifysavingofcloudannotationzoom() throws Exception {
		try {
			Log.testCaseInfo("TC_092 (Viewer annotation level cases): Verify the cloud annotation drawn on the viewer getting saved properly after zoom in the document.");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#shapeToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-cloud"));

			Log.assertThat(viewerScreenPage.drawAnnotationzoomin(markupname, filepath1, parentlocator, childlocator), "The cloud annotation is validated properly inside the viewer","The cloud annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}
	
	
	/**
	 * TC_094 (Viewer annotation level cases): Verify the arrow annotation drawn on the viewer getting saved properly after zoom in the document.
	 * 
	 * @throws Exception
	 * Created by Trinanjwan Sarkar
	 */

	@Test(priority = 20, enabled = true, description = "TC_094 (Viewer annotation level cases): Verify the arrow annotation drawn on the viewer getting saved properly after zoom in the document.")
	public void verifysavingofarrowannotationzoom() throws Exception {
		try {
			Log.testCaseInfo("TC_094 (Viewer annotation level cases): Verify the arrow annotation drawn on the viewer getting saved properly after zoom in the document.");
			String uName = PropertyReader.getProperty("usertri");
			String pWord = PropertyReader.getProperty("passtri");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#lineToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-arrow"));

		    Log.assertThat(viewerScreenPage.drawAnnotationarrowzoomin(markupname, filepath1, parentlocator, childlocator), "The arrow annotation is validated properly inside the viewer", "The arrow annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	
	
	/**
	 * TC_008 (Viewer annotation level cases): Verify Reference Calibrator with line calibrator drawn on the viewer getting saved properly.
	 * 
	 * @throws Exception
	 * Created by Ranadeep Ghosh
	 */

	@Test(priority = 21, enabled = true, description = " TC_008 (Viewer annotation level cases): Verify Reference Calibrator with line calibrator drawn on the viewer getting saved properly ")
	public void verifysavingReferenceCalibratorWithLine() throws Exception 
	{
		try 
		{
			Log.testCaseInfo("TC_008 (Viewer annotation level cases): Verify Reference Calibrator with line calibrator drawn on the viewer getting saved properly");
			
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			
			folderPage = new FolderPage(driver).get();			
			viewerScreenPage = folderPage.openingfileinViewer();
			
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentCalibrator = driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i"));
			WebElement referenceCalibrator = driver.findElement(By.xpath(".//*[@id='calibrationCreate']/a/i"));
			WebElement childCalibrator = driver.findElement(By.xpath(".//*[@id='measurementLineCreate']/a/i"));

			Log.assertThat(viewerScreenPage.verifyDrawAndSaveLineCalibrator(markupname, filepath1, parentCalibrator, referenceCalibrator, childCalibrator ), " Reference Calibrator with line calibrator drawn on the document inside the viewer is getting saved properly ", " Reference Calibrator with line calibrator drawn on the document inside the viewer save is getting failed!!! ", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} 
		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}

	}
	
	
	
	
	/**
	 * TC_101 (Viewer annotation level cases): Verify Reference Calibrator with rectangular calibrator drawn on the viewer getting saved properly.
	 * 
	 * @throws Exception
	 * Created by Ranadeep Ghosh
	 */

	@Test(priority = 22, enabled = true, description = " TC_101 (Viewer annotation level cases): Verify Reference Calibrator with rectangular calibrator drawn on the viewer getting saved properly ")
	public void verifysavingReferenceCalibratorWithRectangle() throws Exception 
	{
		try 
		{
			Log.testCaseInfo("TC_101 (Viewer annotation level cases): Verify Reference Calibrator with rectangular calibrator drawn on the viewer getting saved properly");
			
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			
			folderPage = new FolderPage(driver).get();			
			viewerScreenPage = folderPage.openingfileinViewer();
			
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentCalibrator = driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i"));
			WebElement referenceCalibrator = driver.findElement(By.xpath(".//*[@id='calibrationCreate']/a/i"));
			WebElement childCalibrator = driver.findElement(By.xpath(".//*[@id='measurementSquareCreate']/a/i"));

			Log.assertThat(viewerScreenPage.verifyDrawAndSaveRectangularCalibrator(markupname, filepath1, parentCalibrator, referenceCalibrator, childCalibrator ), " Reference Calibrator with rectangular calibrator drawn on the document inside the viewer is getting saved properly ", " Reference Calibrator with rectangular calibrator drawn on the document inside the viewer save is getting failed!!! ", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} 
		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}

	}
	
	
	
	
	/**
	 * TC_102 (Viewer annotation level cases): Verify Reference Calibrator with circular calibrator drawn on the viewer getting saved properly.
	 * 
	 * @throws Exception
	 * Created by Ranadeep Ghosh
	 */

	@Test(priority = 23, enabled = true, description = " TC_102 (Viewer annotation level cases): Verify Reference Calibrator with circular calibrator drawn on the viewer getting saved properly ")
	public void verifysavingReferenceCalibratorWithCircle() throws Exception 
	{
		try 
		{
			Log.testCaseInfo("TC_102 (Viewer annotation level cases): Verify Reference Calibrator with circular calibrator drawn on the viewer getting saved properly");
			
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			
			folderPage = new FolderPage(driver).get();			
			viewerScreenPage = folderPage.openingfileinViewer();
			
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentCalibrator = driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i"));
			WebElement referenceCalibrator = driver.findElement(By.xpath(".//*[@id='calibrationCreate']/a/i"));
			WebElement childCalibrator = driver.findElement(By.xpath(".//*[@id='measurementCircleCreate']/a/i"));

			Log.assertThat(viewerScreenPage.verifyDrawAndSaveCircularCalibrator(markupname, filepath1, parentCalibrator, referenceCalibrator, childCalibrator ), " Reference Calibrator with circular calibrator drawn on the document inside the viewer is getting saved properly ", " Reference Calibrator with circular calibrator drawn on the document inside the viewer save is getting failed!!! ", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} 
		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}

	}
	
	
	
	
	/**
	 * TC_103 (Viewer annotation level cases): Verify Reference Calibrator with freehand line calibrator drawn on the viewer getting saved properly.
	 * 
	 * @throws Exception
	 * Created by Ranadeep Ghosh
	 */

	@Test(priority = 24, enabled = true, description = " TC_103 (Viewer annotation level cases): Verify Reference Calibrator with freehand line calibrator drawn on the viewer getting saved properly ")
	public void verifysavingReferenceCalibratorWithFreehandLine() throws Exception 
	{
		try 
		{
			Log.testCaseInfo("TC_103 (Viewer annotation level cases): Verify Reference Calibrator with freehand line calibrator drawn on the viewer getting saved properly");
			
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			
			folderPage = new FolderPage(driver).get();			
			viewerScreenPage = folderPage.openingfileinViewer();
			
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentCalibrator = driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i"));
			WebElement referenceCalibrator = driver.findElement(By.xpath(".//*[@id='calibrationCreate']/a/i"));
			WebElement childCalibrator = driver.findElement(By.xpath(".//*[@id='measurementFreehandLengthCreate']/a/i"));

			Log.assertThat(viewerScreenPage.verifyDrawAndSaveFreehandLineCalibrator(markupname, filepath1, parentCalibrator, referenceCalibrator, childCalibrator ), " Reference Calibrator with freehand line calibrator drawn on the document inside the viewer is getting saved properly ", " Reference Calibrator with freehand line calibrator drawn on the document inside the viewer save is getting failed!!! ", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} 
		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}

	}
	
	
	
	
	/**
	 * TC_104 (Viewer annotation level cases): Verify Reference Calibrator with freehand area calibrator drawn on the viewer getting saved properly.
	 * 
	 * @throws Exception
	 * Created by Ranadeep Ghosh
	 */

	@Test(priority = 25, enabled = true, description = " TC_104 (Viewer annotation level cases): Verify Reference Calibrator with freehand area calibrator drawn on the viewer getting saved properly ")
	public void verifysavingReferenceCalibratorWithFreehandarea() throws Exception 
	{
		try 
		{
			Log.testCaseInfo("TC_104 (Viewer annotation level cases): Verify Reference Calibrator with freehand area calibrator drawn on the viewer getting saved properly");
			
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			
			folderPage = new FolderPage(driver).get();			
			viewerScreenPage = folderPage.openingfileinViewer();
			
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentCalibrator = driver.findElement(By.xpath(".//*[@id='measureToolMenuBtn']/i"));
			WebElement referenceCalibrator = driver.findElement(By.xpath(".//*[@id='calibrationCreate']/a/i"));
			WebElement childCalibrator = driver.findElement(By.xpath(".//*[@id='measurementFreehandAreaCreate']/a/i"));

			Log.assertThat(viewerScreenPage.verifyDrawAndSaveFreehandAreaCalibrator(markupname, filepath1, parentCalibrator, referenceCalibrator, childCalibrator ), " Reference Calibrator with freehand area calibrator drawn on the document inside the viewer is getting saved properly ", " Reference Calibrator with freehand area calibrator drawn on the document inside the viewer save is getting failed!!! ", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} 
		catch (Exception e) 
		{
			e.getCause();
			Log.exception(e, driver);
			
		} 
		finally 
		{
			Log.endTestCase();
			driver.quit();
			
		}

	}
	
	/**
	 * TC_098 (Viewer annotation level cases): Verify Free Text Annotation drawn on the viewer getting saved properly after zooming in the document.
	 * 
	 * @throws Exception
	 * Created by Arka Halder
	 */

	@Test(priority = 26, enabled = true, description = "TC_098 (Viewer annotation level cases): Verify Free Text Annotation drawn on the viewer getting saved properly after zooming in the document.")
	public void verifyZoomInFreeTextAnnotationSaving() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_098 (Viewer annotation level cases): Verify Free Text Annotation drawn on the viewer getting saved properly after zooming in the document.");
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#textToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-text"));

			Log.assertThat(viewerScreenPage.drawZoomInFreeTextAnnotation(markupname, filepath1, parentlocator, childlocator), "The freetext annotation is validated properly inside the viewer", "The freetext annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_099 (Viewer annotation level cases): Verify Free Text Note Annotation drawn on the viewer getting saved properly after zooming in the document.
	 * 
	 * @throws Exception
	 * Created by Arka Halder
	 */

	@Test(priority = 27, enabled = true, description = "TC_099 (Viewer annotation level cases): Verify Free Text Note Annotation drawn on the viewer getting saved properly after zooming in the document.")
	public void verifyZoomInFreeTextNoteAnnotationSaving() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_099 (Viewer annotation level cases): Verify Free Text Note Annotation drawn on the viewer getting saved properly after zooming in the document.");
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#textToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-note"));

			Log.assertThat(viewerScreenPage.drawZoomInFreeTextNoteAnnotation(markupname, filepath1, parentlocator, childlocator), "The freetext annotation is validated properly inside the viewer", "The freetext annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}

	/**
	 * TC_100 (Viewer annotation level cases): Verify Free Text Callout Annotation drawn on the viewer getting saved properly after zooming in the document.
	 * 
	 * @throws Exception
	 * Created by Arka Halder
	 */

	@Test(priority = 28, enabled = true, description = "TC_100 (Viewer annotation level cases): Verify Free Text Callout Annotation drawn on the viewer getting saved properly after zooming in the document.")
	public void verifyZoomInFreeCalloutAnnotationSaving() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_100 (Viewer annotation level cases): Verify Free Text Callout Annotation drawn on the viewer getting saved properly after zooming in the document.");
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector("#textToolMenuBtn"));
			WebElement childlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-callout"));

			Log.assertThat(viewerScreenPage.drawZoomInFreeTextCalloutAnnotation(markupname, filepath1, parentlocator, childlocator), "The freetext annotation is validated properly inside the viewer", "The freetext annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}

	}
	
	/**
	 * TC_097 (Viewer annotation level cases): Verify Rectangular Highlight Annotation drawn on the viewer getting saved properly after zooming in the document.
	 * 
	 * @throws Exception
	 * Created by Arka Halder
	 */

	@Test(priority = 29, enabled = true, description = "TC_097 (Viewer annotation level cases): Verify Rectangular Highlight Annotation drawn on the viewer getting saved properly after zooming in the document.")
	public void verifyZoomInRectangularHighlightAnnotationSaving() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_097 (Viewer annotation level cases): Verify Rectangular Highlight Annotation drawn on the viewer getting saved properly after zooming in the document.");
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingfileinViewer();
			String markupname = viewerScreenPage.Random_Markupname();
			File fis1 = new File(PropertyReader.getProperty("SaveScreenshot"));
			String filepath1 = fis1.getAbsolutePath().toString();

			WebElement parentlocator = driver.findElement(By.cssSelector(".leaflet-draw-draw-rectHighlighter"));

			Log.assertThat(viewerScreenPage.drawZoomInRectangularHighlightAnnotation(markupname, filepath1, parentlocator), "The freetext annotation is validated properly inside the viewer", "The freetext annotation validation got failed", driver);
			viewerScreenPage.deletemarkup(markupname);
			viewerScreenPage.delteScreenShot(filepath1);

		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/**
	 * TC_105 (Viewer annotation level cases): Verify 90 degree rotation in viewer in both clockwise and anti-clockwise direction.
	 * 
	 * @throws Exception
	 * Created by Arka Halder
	 */

	@Test(priority = 30, enabled = true, description = "TC_105 (Viewer annotation level cases): Verify 90 degree rotation in viewer in both clockwise and anti-clockwise direction.")
	public void verify90DegreeRotation() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_105 (Viewer annotation level cases): Verify 90 degree rotation in viewer in both clockwise and anti-clockwise direction.");
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingFullFileInViewer();

			String referencePath = new File(PropertyReader.getProperty("Screenshotpath")).getAbsolutePath().toString();
			String screenshotPath = new File(PropertyReader.getProperty("SaveScreenshot")).getAbsolutePath().toString();
			WebElement rotateMenu = driver.findElement(By.cssSelector(".leaflet-draw-draw-rotate"));
			Log.assertThat(viewerScreenPage.validateRotation(referencePath, screenshotPath, rotateMenu, 90),
					"+90 degree and -90 degree rotations have been validated properly inside the viewer",
					"Validation for +90 degree and -90 degree rotations has failed inside the viewer", driver);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/**
	 * TC_106 (Viewer annotation level cases): Verify 180 degree rotation in viewer in both clockwise and anti-clockwise direction.
	 * 
	 * @throws Exception
	 * Created by Arka Halder
	 */

	@Test(priority = 31, enabled = true, description = "TC_106 (Viewer annotation level cases): Verify 180 degree rotation in viewer in both clockwise and anti-clockwise direction.")
	public void verify180DegreeRotation() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_106 (Viewer annotation level cases): Verify 180 degree rotation in viewer in both clockwise and anti-clockwise direction.");
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingFullFileInViewer();

			String referencePath = new File(PropertyReader.getProperty("Screenshotpath")).getAbsolutePath().toString();
			String screenshotPath = new File(PropertyReader.getProperty("SaveScreenshot")).getAbsolutePath().toString();
			WebElement rotateMenu = driver.findElement(By.cssSelector(".leaflet-draw-draw-rotate"));
			Log.assertThat(viewerScreenPage.validateRotation(referencePath, screenshotPath, rotateMenu, 180),
					"+180 degree and -180 degree rotations have been validated properly inside the viewer",
					"Validation for +180 degree and -180 degree rotations has failed inside the viewer", driver);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}

	/**
	 * TC_107 (Viewer annotation level cases): Verify 270 degree rotation in viewer in both clockwise and anti-clockwise direction.
	 * 
	 * @throws Exception
	 * Created by Arka Halder
	 */

	@Test(priority = 32, enabled = true, description = "TC_107 (Viewer annotation level cases): Verify 270 degree rotation in viewer in both clockwise and anti-clockwise direction.")
	public void verify270DegreeRotation() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_107 (Viewer annotation level cases): Verify 270 degree rotation in viewer in both clockwise and anti-clockwise direction.");
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingFullFileInViewer();

			String referencePath = new File(PropertyReader.getProperty("Screenshotpath")).getAbsolutePath().toString();
			String screenshotPath = new File(PropertyReader.getProperty("SaveScreenshot")).getAbsolutePath().toString();
			WebElement rotateMenu = driver.findElement(By.cssSelector(".leaflet-draw-draw-rotate"));
			Log.assertThat(viewerScreenPage.validateRotation(referencePath, screenshotPath, rotateMenu, 270),
					"+270 degree and -270 degree rotations have been validated properly inside the viewer",
					"Validation for +270 degree and -270 degree rotations has failed inside the viewer", driver);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/**
	 * TC_108 (Viewer annotation level cases): Verify 360 degree rotation error message in clockwise direction.
	 * 
	 * @throws Exception
	 * Created by Arka Halder
	 */

	@Test(priority = 33, enabled = true, description = "TC_108 (Viewer annotation level cases): Verify 360 degree rotation error message in clockwise direction.")
	public void verify360DegreeRotation() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_108 (Viewer annotation level cases): Verify 360 degree rotation error message in clockwise direction.");
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingFullFileInViewer();

			WebElement rotateMenu = driver.findElement(By.cssSelector(".leaflet-draw-draw-rotate"));
			Log.assertThat(viewerScreenPage.validate360Rotation(rotateMenu, 360),
					"+360 degree rotation error message have been validated properly inside the viewer",
					"+360 degree rotation error message is not meeting the expectation inside the viewer", driver);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/**
	 * TC_109 (Viewer annotation level cases): Verify -360 degree rotation error message in clockwise direction.
	 * 
	 * @throws Exception
	 * Created by Arka Halder
	 */
	
	@Test(priority = 34, enabled = true, description = "TC_109 (Viewer annotation level cases): Verify -360 degree rotation error message in clockwise direction.")
	public void verifyMinus360DegreeRotation() throws Exception {
		try {
			Log.testCaseInfo(
					"TC_109 (Viewer annotation level cases): Verify -360 degree rotation error message in clockwise direction.");
			String uName = PropertyReader.getProperty("Viewertestuser");
			String pWord = PropertyReader.getProperty("Viewertestuserpassword");

			projectsLoginPage = new ProjectsLoginPage(driver).get();
			projectDashboardPage = projectsLoginPage.loginWithValidCredential(uName, pWord);
			projectDashboardPage.enterviewerlevelProject();
			folderPage = new FolderPage(driver).get();
			viewerScreenPage = folderPage.openingFullFileInViewer();

			WebElement rotateMenu = driver.findElement(By.cssSelector(".leaflet-draw-draw-rotate"));
			Log.assertThat(viewerScreenPage.validate360Rotation(rotateMenu, -360),
					"+360 degree rotation error message have been validated properly inside the viewer",
					"+360 degree rotation error message is not meeting the expectation inside the viewer", driver);
		} catch (Exception e) {
			e.getCause();
			Log.exception(e, driver);
		} finally {
			Log.endTestCase();
			driver.quit();
		}
	}
	
}