package com.arc.projects.testscripts;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arc.projects.pages.FolderPage;
import com.arc.projects.pages.ProjectAndFolder_Level_Search;
import com.arc.projects.pages.ProjectDashboardPage;
import com.arc.projects.pages.ProjectsLoginPage;
import com.arc.projects.pages.ViewerScreenPage;
import com.arc.projects.utils.Generate_Random_Number;
import com.arc.projects.utils.PropertyReader;
import com.arc.projects.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;

public class ViewerPage_PUNCH_Testcases {
	
	public static WebDriver driver;
    
	ProjectsLoginPage projectsLoginPage;
    ProjectDashboardPage projectDashboardPage;   
    ViewerScreenPage viewerScreenPage; 
    FolderPage folderPage;
   ProjectAndFolder_Level_Search projectAndFolder_Level_Search;
    
    @Parameters("browser")
    @BeforeMethod
    public WebDriver beforeTest(String browser) {
        
    	if(browser.equalsIgnoreCase("firefox")) 
        {
               File dest = new File("./drivers/win/geckodriver.exe");
               //System.setProperty("webdriver.gecko.driver", dest.getAbsolutePath());
               System.setProperty("webdriver.firefox.marionette", dest.getAbsolutePath());
               driver = new FirefoxDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
        
        else if (browser.equalsIgnoreCase("chrome")) 
        { 
        File dest = new File("./drivers/win/chromedriver.exe");
        System.setProperty("webdriver.chrome.driver", dest.getAbsolutePath());  
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("download.default_directory",  "C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+ File.separator + "Downloads");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-maximized");
        options.setExperimentalOption("prefs", prefs);
        driver = new ChromeDriver( options );
        driver.get(PropertyReader.getProperty("SkysiteProdURL"));
 
        } 
        
        else if (browser.equalsIgnoreCase("safari"))
        {
               System.setProperty("webdriver.safari.noinstall", "true"); //To stop uninstall each time
               driver = new SafariDriver();
               driver.get(PropertyReader.getProperty("SkysiteProdURL"));
        }
   return driver;
 }
	
	
    
    
    /** TC_029(Viewer Page): Punch creation with Internal attachments, external attachments adding attributes employee in �To� section CC is shared user 
    Recipient is uploading the new revision of the document.
  *
  **/
  @Test(priority = 0, enabled =true, description = "Recipient is uploading the new revision of the document")
  public void Verify_Recipientis_uploadingthe_newrevision_document() throws Throwable
  {

  try
  {
  Log.testCaseInfo("TC_029 (Multiple ToolsBaar validation): Recipient is uploading the new revision of the document.");
  String uName = PropertyReader.getProperty("Username1");
  String pWord = PropertyReader.getProperty("Password1");
  String uName1 = PropertyReader.getProperty("Username2");
  String pWord1 = PropertyReader.getProperty("Password2");
  //==============================Login with valid UserID/PassWord=====================
  projectsLoginPage = new ProjectsLoginPage(driver).get();  
  projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);     		

  /*//=========Create Project Randomly===================================
  String Project_Name = "Project_"+Generate_Random_Number.MyDate();
  folderPage=projectDashboardPage.CreateProject(Project_Name);

  //=========Create Folder Randomly===================================
  String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
  folderPage.New_Folder_Create(Foldername);      		
  folderPage.Select_Folder(Foldername);

  //================File Upload without Index=====================
  String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
  String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
  int FileCount = Integer.parseInt(CountOfFilesInFolder);
  viewerScreenPage = new ViewerScreenPage(driver).get();
  folderPage.Upload_WithoutIndex(FolderPath, FileCount)  ; 
  viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
  folderPage.UploadFiles_WithoutIndexMycomputer(FolderPath, FileCount);
  viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);*/
  
//Select Project
String Project_Name = "Specific_Testdata_Static";
folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
//select folder			
String Foldername = "RFI_Folder";
folderPage.Select_Folder(Foldername);
			
  //=======Owner Level Punch Validation========================
 viewerScreenPage = new ViewerScreenPage(driver).get();
 String parentHandle = driver.getWindowHandle();
 viewerScreenPage.Image1();
 viewerScreenPage.PunchValidation_creation1(parentHandle);
 driver.close();
 driver.switchTo().window(parentHandle);
 
  String parentHandle1 = driver.getWindowHandle();
  viewerScreenPage.Image2();
  viewerScreenPage.PunchValidation_creation2(parentHandle1);
  driver.close();
  driver.switchTo().window(parentHandle1);
  Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);

  //=========Employee Level Validation=============     		     		
  projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName1,pWord1);  
  projectDashboardPage.ValidateSelectProject(Project_Name);
  folderPage.Select_Folder(Foldername);  
  viewerScreenPage.Image1();
  String parentHandle2 = driver.getWindowHandle();
  viewerScreenPage.PUNCH_EmployeeLevel_VALIDATION( parentHandle2);
  driver.close();
  driver.switchTo().window(parentHandle2);	        
  viewerScreenPage.Image2();
  String parentHandle3 = driver.getWindowHandle();
  viewerScreenPage.PUNCH_EmployeeLevel_VALIDATION1( parentHandle3);
  driver.close();
  driver.switchTo().window(parentHandle3);
  Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);


  }
  catch(Exception e)
  {
  e.getCause();
  Log.exception(e, driver);
  }
  finally
  {
  Log.endTestCase();
  driver.close();
  }

  }
    
      
    
   

    
    
    
    
    
    
    
    
    
    
    
    
    /*Test case : TC_063.Navigate from RFI listings and try to create PUNCH annotation.
     * Create one in document level in online mode with To, Cc, Subject, disciple, sheet number, custom attributes, specification, potential cost impact(checked), potential schedule impact(checked), Question and attachment(external and internal)
       1) Navigate to the document and validate the document.
       2) Validate the postion and content of RFI.
       3) Close the pop over and tap on view all and validate the position of mark ups.
       4) Create RFI, Punch and photo annotation on the document an validate those in web.
     */

    @Test(priority = 1, enabled = true, description = "Navigate from RFI listings and try to create PUNCH")
    public void NavigatefromRFIListingtocreatePunch() throws Throwable
         {         	 
         try
         {
             Log.testCaseInfo("TC_063 (RFI Related Testcases):Navigate from RFI listings and try to create PUNCH");
             String uName = PropertyReader.getProperty("Username1");
             String pWord = PropertyReader.getProperty("Password1");
             //==============Login with valid UserID/PassWord===========================
             projectsLoginPage = new ProjectsLoginPage(driver).get();  
             projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);//Calling Login MethodS	           		
            /* //=========Create Project Randomly===================================
             String Project_Name = "Projet_"+Generate_Random_Number.generateRandomValue();
             folderPage=projectDashboardPage.CreateProject(Project_Name);         		
             //=========Create Folder Randomly===================================
             String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
             folderPage.New_Folder_Create(Foldername); 	
             folderPage.Select_Folder(Foldername);         		
             //================File Upload without Index=====================
             viewerScreenPage = new ViewerScreenPage(driver).get();
             String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
             String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_DonotInd");
             int FileCount = Integer.parseInt(CountOfFilesInFolder);
             folderPage.Upload_WithoutIndex(FolderPath, FileCount);     		
             viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
            */
             //Select Project
             String Project_Name = "Specific_Testdata_Static";
             folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
             //select folder			
             String Foldername = "RFI_Folder";
             folderPage.Select_Folder(Foldername);
             viewerScreenPage = new ViewerScreenPage(driver).get();
             //============Punch Creation ON first File===========================
             String parentHandle = driver.getWindowHandle();
    		 viewerScreenPage.Image1();
    		 viewerScreenPage.PunchValidation_creation1(parentHandle);
    		// viewerScreenPage.ValidationAfterPunchCreation();
    		 driver.close();		 			 
    		 driver.switchTo().window(parentHandle);		 
    		 viewerScreenPage.Logout(); 
    		  	       			
              		
              	}
                  catch(Exception e)
                  {
                  	e.getCause();
                  	Log.exception(e, driver);
                  }
              	finally
              	{
              		Log.endTestCase();
              		driver.close();
              	}
           	
           }  
	
	/** TC_027(Viewer Page):.Punch creation with Internal attachments, external attachments adding attributes employee in �To� section CC is shared user 
    Recipient opening the same posting comments and attaching a document then completing the punch then owner closing the punch.
    **/
   @Test(priority = 2, enabled =true, description = "Recipient opening the same posting comments and attaching a document then completing the punch then owner closing the punch.")
   public void Verify_RecipientUploading_newdocumentAnd() throws Throwable
   {
  	 
   	
	   try
   	{
  Log.testCaseInfo("TC_027 (Multiple ToolsBaar validation): Recipient opening the same posting comments and attaching a document then completing the punch then owner closing the punch.");
  String uName = PropertyReader.getProperty("Username1");
  String pWord = PropertyReader.getProperty("Password1");
  String uName1 = PropertyReader.getProperty("Username2");
  String pWord1 = PropertyReader.getProperty("Password2");
  //String Project_Name = "Project_10580";
  //String Foldername="Folder_26543";
  //==============================Login with valid UserID/PassWord=====================
  projectsLoginPage = new ProjectsLoginPage(driver).get();  
  projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);
    		
 /*//=========Create Project Randomly====================================================
  String Project_Name = "Project_"+Generate_Random_Number.generateRandomValue();
  folderPage=projectDashboardPage.CreateProject(Project_Name);
    		
  //=========Create Folder Randomly===================================
   String Foldername = "Folder_"+Generate_Random_Number.generateRandomValue();
   folderPage.New_Folder_Create(Foldername);      		
   folderPage.Select_Folder(Foldername);    		
   //================File Upload without Index=====================
   String FolderPath = PropertyReader.getProperty("Upload_TestData_viewer_SingleFile");
   String CountOfFilesInFolder = PropertyReader.getProperty("FileCount_viewer");
   int FileCount = Integer.parseInt(CountOfFilesInFolder);
   folderPage.Upload_WithoutIndex(FolderPath, FileCount); */   
  //Select Project
  String Project_Name = "Specific_Testdata_Static";
  folderPage = projectDashboardPage.ValidateSelectProject(Project_Name);
  //select folder			
  String Foldername = "RFI_Folder";
  folderPage.Select_Folder(Foldername);
  viewerScreenPage = new ViewerScreenPage(driver).get();
   //=======owner Validation========================  			         		
   viewerScreenPage = new ViewerScreenPage(driver).get(); 
   //viewerScreenPage.NavigatonBacktodeshboard(Project_Name, Foldername);
   String parentHandle = driver.getWindowHandle();	        		
   viewerScreenPage.Image1();
   viewerScreenPage.PunchValidation_creation1(parentHandle);
   driver.close();
   driver.switchTo().window(parentHandle);
   Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);
   //=========Employee Level Validation=============     		     		
   projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName1,pWord1);  
   projectDashboardPage.ValidateSelectProject(Project_Name) ;
   folderPage.Select_Folder(Foldername);  
   viewerScreenPage.Image1();
   viewerScreenPage.PUNCH_EmployeeLevel_VALIDATION( parentHandle);
   driver.close();
   driver.switchTo().window(parentHandle);
   Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);
   //========== owner level Closed Punch=============			 
   
    projectDashboardPage=projectsLoginPage.loginWithValidCredential(uName,pWord);  
    projectDashboardPage.ValidateSelectProject(Project_Name);
   	folderPage.Select_Folder(Foldername);	
    viewerScreenPage.Image1();
    //String parentHandle = driver.getWindowHandle();	
   	viewerScreenPage.PUNCH_Closed_OwnerLevel(parentHandle);
   	driver.close();
    driver.switchTo().window(parentHandle);
	Log.assertThat( viewerScreenPage.Logout(), "Logout working Successfully","Logout not working Successfully", driver);
			 
   	}
       catch(Exception e)
       {
       	e.getCause();
       	Log.exception(e, driver);
       }
   	finally
   	{
   		Log.endTestCase();
   		driver.close();
   	}
   	
   }
   	

}
